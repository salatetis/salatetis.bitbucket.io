<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Here at Unlikely Models Camp we want to create a unique, close-knit community of girls from a variety of backgrounds and interests. So give us the chance to get to know you better and, hopefully, we will meet this summer at Igate castle!">
    <link rel="stylesheet" href="public/styles/main.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
    <title>Models Camp</title>
</head>
<body>
<div class="menu-container">
    <!-- Everything below this is part of the menu-->
    <div class="overlay" id="overlay">
        <nav class="overlay-menu">
            <ul>
                <li><a href="#join" class="scroll_to">JOIN THE CAMP!</a></li>
                <li><a href="#story" class="scroll_to">READ THE STORY</a></li>
                <li><a href="#map" class="scroll_to">SEE ON MAP</a></li>
                <li><a href="#contacts" class="scroll_to">CONTACTS</a></li>
            </ul>
        </nav>
    </div>
</div>

<!-- Main container -->
<div id="navBar" class="section is-paddingless menu">
    <div class="container">
        <div class="content has-text-centered">
            <a class="button is-black button-large">MODEL'S CAMP</a>
            <div class="button-container" id="toggle">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </div>
        </div>
    </div>
</div>

<div id="join" class="section is-medium is-paddingless">
    <div class="container has-text-centered">
        <div class="columns is-gapless is-centered is-vcentered">
            <div class="column">
                <div class="content" id="join-slogan">
                    <figure class="image slide-in-left">
                        <img src="./public/assets/images/callout_title.svg">
                    </figure>
                    <a id="join-button" class="button is-black button-large flip-in-hor-bottom">JOIN THE CAMP!!</a>
                    <br><br>
                </div>
            </div>
            <div class="column is-three-fifths-desktop-only is-invisible-mobile">
            </div>
        </div>
    </div>
    <figure class="image" id="join-spot-01">
        <img src="./public/assets/images/join_bg_02.svg">
    </figure>
    <div id="modal-join" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <button class="modal-close"></button>
            </header>
            <section class="modal-card-body">
                <form method="post" action="#" class="toggle-disabled has-validation-callback disabled-without-errors" enctype="multipart/form-data" id="registration-form">
                    <div class="content">
                        <div class="columns">
                            <div class="column">
                                <div class="content has-text-centered">
                                    <p class="subtitle is-1 has-text-white">JOIN THE CAMP!</p>
                                    <p class="has-text-white">Here at Unlikely Models Camp we want to create a unique, close-knit community of girls from a variety of backgrounds
                                        and interests. So give us the chance to get to know you better and, hopefully, we will meet this summer at Igate castle!</p>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="column">
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <label class="required">
                                                <input class="input is-uppercase is-large" type="text" data-validation="custom" data-validation-regexp="^([A-Za-z]+)$"
                                                       maxlength="50" name="name" placeholder="Name" required>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column">
                                <div class="field">
                                    <div class="control is-expanded">
                                        <label class="required">
                                            <input class="input is-uppercase is-large" type="text" data-validation="custom" data-validation-regexp="^([A-Za-z]+)$" maxlength="50"
                                                   name="surname" placeholder="Surname">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="column">
                                <div class="field is-horizontal">
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input is-uppercase is-large" type="text" data-validation="number" data-validation-allowing="range[1;31]" maxlength="2"
                                                       name="date" placeholder="DD" required/>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="control">
                                                <input class="input is-uppercase is-large" type="text" data-validation="number" data-validation-allowing="range[1;12]" maxlength="2"
                                                       name="month" placeholder="MM" required/>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="control">
                                                <label class="required">
                                                    <input class="input is-uppercase is-large" type="text" data-validation="number" data-validation-allowing="range[1906;2013]"
                                                           maxlength="4" name="year" placeholder="YYYY" required/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column">
                                <div class="field" style="width: 200px;">
                                    <div class="control">
                                        <label class="required">
                                            <input class="input is-uppercase is-large" type="text" data-validation="number" data-validation-allowing="range[50;250]" maxlength="3"
                                                   name="height" placeholder="Height (cm)" required>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="column">
                                <div class="field">
                                    <div class="control">
                                        <label class="required">
                                            <input class="input is-uppercase is-large" type="text" data-validation="email" maxlength="50" name="email" placeholder="Email"
                                                   required/>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="column">
                                <div class="field">
                                    <div class="control">
                                        <label class="required">
                                            <input class="input is-uppercase is-large" type="text" data-validation="number" data-validation-allowing="number" name="phone"
                                                   placeholder="Phone" required/>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="column">
                                <label class="label has-text-white is-size-6">instagram.com/</label>
                                <div class="field">
                                    <div class="control is-expanded">
                                        <input class="input is-uppercase is-large" type="text" maxlength="50" name="instahandle" placeholder="Instagram handle">
                                    </div>
                                </div>
                            </div>
                            <div class="column is-right">
                                <label class="label has-text-white is-size-6">Portrait photo</label>
                                <div class="file is-boxed is-fullwidth">
                                    <label class="file-label has-text-centered">
                                        <input class="file-input" type="file" accept=".jpg,.jpeg,.png" name="portrait_pic">
                                        <span class="file-cta">
                                        <span class="file-label is-uppercase is-small">Choose a file…</span>
                                    </span>
                                    </label>
                                </div>
                            </div>
                            <div class="column is-right">
                                <label class="label has-text-white is-size-6">Full body photo</label>
                                <div class="file is-boxed is-fullwidth">
                                    <label class="file-label has-text-centered">
                                        <input class="file-input" type="file" name="body_pic">
                                        <span class="file-cta">
                                        <span class="file-label is-uppercase is-small">Choose a file…</span>
                                    </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="column">
                                <label class="label has-text-grey-light is-size-7">Although linking your Instagram account is preferable, if you do not actively use Instagram or
                                    prefer to not have it public, please upload two recent photos of you instead.</label>
                                <br>
                                <div class="field">
                                    <label class="label has-text-white is-size-6">If you could choose anyone, who would you pick as your mentor?</label>
                                        <div class="field">
                                            <div class="control">
                                                <textarea class="textarea is-large is-uppercase" maxlength="500" rows="6" name="text_mentor" placeholder="Max 500 chars"></textarea>
                                            </div>
                                        </div>
                                </div>
                                <div class="field">
                                    <label class="label has-text-white is-size-6">What are you known for amongst your friends?</label>
                                        <div class="field">
                                            <div class="control">
                                                <textarea class="textarea is-large is-uppercase" maxlength="500" rows="6" name="text_knownfor" placeholder="Max 500 chars"></textarea>
                                            </div>
                                        </div>
                                </div>
                                <div class="field">
                                    <label class="label has-text-white is-size-6">What is on your bucket list?</label>
                                        <div class="field">
                                            <div class="control">
                                                <textarea class="textarea is-large is-uppercase" maxlength="500" rows="6" name="text_bucket_list" placeholder="Max 500 chars"></textarea>
                                            </div>
                                        </div>
                                </div>
                                <div class="file is-boxed is-centered">
                                    <input type="submit" value="JOIN!" name="submitdata">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
            <div class="modal-card-foot"></div>
        </div>
    </div>
</div>


<div id="story" class="section">
    <div class="container has-text-centered">
        <div class="columns is-gapless is-vcentered reverse-column-order">
            <div class="column">
                <div class="content is-medium" id="story-text">
                    <p>In 2014, she was the world’s 27 th best paid model. She has walked the world’s most elite
                        runways and was featured in world’s most respected magazines. You might fall asleep while I
                        list all of her achievements – Ginta Lapiņa is a world renown top model, after all.
                        <br>But an unlikely one.</p>
                </div>
            </div>
            <div class="column is-three-fifths-desktop-only">
                <p id="story-slogan" class="subtitle is-1 has-text-black-bis focus-in-contract-bck">LEARN <br>FROM <br> GINTA LAPINA</p>
                <br>
            </div>
        </div>
        <div class="columns is-paddingless is-gapless">
            <div class="column is-hidden-mobile"></div>
            <div class="column is-three-fifths-desktop-only">
                <a id="story-button" class="button is-black button-large button-large">READ THE STORY</a>
            </div>
        </div>
    </div>
</div>

<div id="story-hidden" class="section" style="display: none;">
    <div class="container has-text-centered">
        <div class="columns">
            <div class="column">
                <div class="content">
                    <p>Today she is amongst the best, but her unlikely career began in one of Riga’s trolleybuses, when a modeling scout spotted her. </p>
                    <p>Five years later, she cemented her status in fashion industry by becoming the face of Yves Saint Laurent beauty, Miu Miu, Marc by Marc Jacobs, Nars cosmetics and Dolce & Gabbana skincare, to name just a few. Through her persistency and hard work, she got through the mistakes and the rejection. And, man, does she have a story to tell and advice to share! Her original scout gave her the opportunity to pursue her modeling career, and now she wants to create this opportunity for the next unlikely model.</p>
                </div>
            </div>
            <div class="column">
                <div class="content">
                    <p>This is not a competition, nor a boot camp for becoming a top model. It is something deeper and more meaningful than that. A sisterhood of girls who empower each other and learn to find themselves through new experiences. With the guidance of Ginta and industry’s leading experts, you will be given insights into nutrition, healthy lifestyle and photography. And even if you decide not to become a model, you will have seen yourself from a different perspective, and learned to love yourself a little bit more.</p>
                </div>
            </div>
            <br><br>
        </div>
    </div>
</div>

<div id="team-header" class="section">
    <div class="container">
        <figure class="image">
            <img src="./public/assets/images/team_bg.svg">
        </figure>
    </div>
</div>

<div id="team" class="section is-medium is-paddingless">
    <div class="container has-text-centered">
        <div class="columns">
            <div class="column">
                <div class="content">
                    <br>
                    <p class="subtitle is-1 has-text-white focus-in-contract-bck">A TEAM <br>OF <br>EXPERTS</p>
                </div>
            </div>
            <div class="column is-three-fifths-desktop-only">
                <div id="team-text" class="content is-medium">
                    <p>There’s a lot of mystery behind the modeling business, many invisible hands contributing to each final result. To uncover the reality, you will learn directly from fashion industry’s leading experts working with elite models. Each of them handpicked by Ginta, they will train you, teach you, inspire you and challenge you.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="team-footer" class="section">
    <div class="container">
        <figure class="image">
            <img src="./public/assets/images/team_footer_bg1.png">
        </figure>
    </div>
</div>

<div id="map" class="section">
    <div class="container has-text-centered">
        <div class="columns is-gapless reverse-column-order">
            <div class="column">
                <div  id="map-text" class="content is-medium">
                    <p>Always thought that you cannot start your modeling career without the Eiffel tower as your backdrop or without hustling through Manhattan in a yellow cab? Don’t be ridiculous! With a camp so unique, the location must be extraordinary as well.</p>
                    <p>Igate Castle might be an unlikely destination, but what better place to find your poise, than a castle?</p>
                </div>
            </div>
            <div class="column is-three-fifths-desktop-only">
                <div class="content">
                    <br>
                    <p class="subtitle is-1 has-text-black-bis focus-in-contract-bck">THE CASTLE</p>
                </div>
            </div>
        </div>
        <div class="columns is-gapless is-hidden-mobile">
            <div class="column">
                <a class="button is-black button-large" target="_blank" href="https://goo.gl/maps/uMzimFtZh872">SEE ON MAP</a>
            </div>
            <div class="column is-three-fifths-desktop-only"></div>
        </div>
    </div>
</div>

<div id="map-promo" class="section is-hidden-desktop">
    <div>
        <figure class="image">
            <img src="./public/assets/images/castle_bg.png">
        </figure>
    </div>
    <div class="columns has-text-centered">
        <div class="column">
            <a class="button is-black button-large" target="_blank" href="https://goo.gl/maps/uMzimFtZh872">SEE ON MAP</a>
        </div>
    </div>
</div>

<footer class="footer" id="contacts">
    <div class="container">
        <div class="columns">
            <div class="column is-11 is-offset-1">
                <div class="columns is-mobile is-vcentered">
                    <div class="column is-narrow is-paddingless">
                        <figure class="image is-48x48">
                            <a href="https://www.facebook.com/unlikelymodel/" target="_blank"><img src="./public/assets/images/icons/icon_facebook.png"></a>
                        </figure>
                    </div>
                    <div class="column is-narrow is-paddingless">
                        <figure class="image is-48x48">
                            <a href=" https://www.instagram.com/unlikelymodelscamp/" target="_blank"><img src="./public/assets/images/icons/icon_instagram.png"></a>
                        </figure>
                    </div>
                    <div class="column is-narrow is-paddingless">
                        <figure class="image is-48x48">
                            <a href="mailto:unlikelymodelscamp@gmail.com" target="_top""><img src="./public/assets/images/icons/icon_mail.png"></a>
                        </figure>
                    </div>
                    <div class="column is-narrow is-paddingless">
                        <figure class="image is-48x48">
                            <a href="https://www.youtube.com/watch?v=GbpQVYJr-Kg" target="_blank""><img src="./public/assets/images/icons/icon_youtube.png"></a>
                        </figure>
                    </div>
                    <div class="column">
                        <p class="subtitle is-7 is-pulled-right"><strong class="has-text-white">Copyright © 2018 MODEL'S CAMP</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php require 'sendmail.php'; ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="lib/main.js"></script>
</body>
</html>