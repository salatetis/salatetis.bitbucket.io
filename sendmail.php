<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader

if(isset($_POST["submitdata"])){

    require 'vendor/autoload.php';
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $age = $_POST['date'].'/'.$_POST['month'].'/'.$_POST['year'];
    $height = $_POST['height'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $instahandle = $_POST['instahandle'];
    $text_mentor = $_POST['text_mentor'];
    $text_knownfor = $_POST['text_knownfor'];
    $text_bucket_list = $_POST['text_bucket_list'];
    $portrait_path = "";
    $body_path = "";

    if(!empty($_FILES["portrait_pic"]["name"])){
        $portrait_path = 'upload/' . $_FILES["portrait_pic"]["name"];
        move_uploaded_file($_FILES["portrait_pic"]["tmp_name"], $portrait_path);
    }

    if(!empty($_FILES["body_pic"]["name"])){
        $body_path = 'upload/' . $_FILES["body_pic"]["name"];
        move_uploaded_file($_FILES["body_pic"]["tmp_name"], $body_path);
    }

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'Unlikelymodelscamp@gmail.com';                 // SMTP username
        $mail->Password = 'Unlikelymodels';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($email, $name." ".$surname);
        $mail->addAddress('Unlikelymodelscamp@gmail.com', 'Ginta Lapina');     // Add a recipient

        //Attachments
        if($portrait_path) {
            $mail->addAttachment($portrait_path);         // Add attachments
        }
        if($body_path) {
            $mail->addAttachment($body_path);
        }
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Request to join Unlikely Models Camp!';
        $mail->Body    = '
        <table style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #222;">
        <tr>
            <td height="30">Name:</td>
            <td height="30">'.$name." ".$surname.'</td>
        </tr>
        <tr>
            <td height="30">Age:</td>
            <td height="30">'.$age.'</td>
        </tr>
        <tr>
            <td height="30">Height:</td>
            <td height="30">'.$height.'cm</td>
        </tr>
        <tr>
            <td height="30">Email:</td>
            <td height="30">'.$email.'</td>
        </tr>
        <tr>
            <td height="30">Phone:</td>
            <td height="30">'.$phone.'</td>
        </tr>
        <tr>
            <td height="30">Instagram handle:</td>
            <td height="30"><a href="http://instagram.com/"'.$instahandle.' target="_blank">instagram.com/'.$instahandle.'</a></td>
        </tr>
    </table>
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #222; width: 600px;">
        <hr>
        <h3><b>If i could choose anyone, who would you pick as your mentor?</b></h3>
        <p>'.$text_mentor.'</p>
    </div>
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #222; width: 600px;">
        <hr>
        <h3><b>What are you known for amongst your friends?</b></h3>
        <p>'.$text_knownfor.'</p>
    </div>
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #222; width: 600px;">
        <hr>
        <h3><b>What is on your bucket list?</b></h3>
        <p>'.$text_bucket_list.'</p>
    </div>';

        $mail->AltBody = strip_tags($mail->Body);

        if($mail->send()) {

            if($portrait_path) {
                unlink($portrait_path);
            }
            if($body_path) {
                unlink($body_path);
            }

            ob_clean();

            //header("location: index.php?sent");
        }

        echo ' <div id="note"> Thanks submission received! We will contact you.<a id="closenote">[close]</a></div>';
    }
    catch (Exception $e) {
        echo ' <div id="note" style="background-color: orangered"> Message could not be sent. Mailer Error:, '.$mail->ErrorInfo.'<a id="closenote">[close]</a></div>';
    }
}