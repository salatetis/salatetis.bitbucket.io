$('#toggle').click(function() {
    $(this).toggleClass('active');
    $('#overlay').toggleClass('open');
});

$('#join-button').click(function() {
    $('#modal-join').addClass('is-active');
    $('#modal-join').addClass('fade-in-fast');
});

$('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button').click(function() {
    $('#modal-join').removeClass('is-active');
    $('#modal-join').removeClass('fade-in-fast');
    $('#modal-join').removeClass('is-clipped');
});


$('#overlay').click(function() {
    $(this).toggleClass('open');
    $('#toggle').toggleClass('active');
});

$('.scroll_to').click(function(e){
    var jump = $(this).attr('href');
    var new_position = $(jump).offset();
    $('html, body').stop().animate({ scrollTop: new_position.top }, 500);
    e.preventDefault();
});


$('#story-button').click(function() {
    $("#story-hidden").slideToggle( "slow");
});


function getAll(selector) {
    return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}

'use strict';

;( function ( document, window, index )
{
    var inputs = document.querySelectorAll( '.file-input' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label	 = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            if(this.files[0].size > 2000000){
                alert("File is too big! Must be less than 2MB");
                this.value = "";
                return;
            };

            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                //label.querySelector( 'span' ).innerHTML = fileName;
                label.querySelector( 'span' ).innerHTML = "FILE READY FOR UPLOAD";
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    });


}( document, window, 0 ));

$.validate({
    modules : 'toggleDisabled',
    disabledFormFilter : 'form.toggle-disabled',
    showErrorDialogs : false
});

/////////////// menu scroll animation

var prev = 0;
var $window = $(window);
var nav = $('.menu');

$window.on('scroll', function(){

    if($('#overlay').hasClass('open')){
        $('#overlay').toggleClass('open');
        $('#toggle').toggleClass('active');
    }

    var scrollTop = $window.scrollTop();
    nav.toggleClass('hidden', scrollTop > prev);
    prev = scrollTop;
});
